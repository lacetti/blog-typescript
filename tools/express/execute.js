const express = require('express')
const path = require('path')
const webpack = require('webpack')
const webpackConfiguration = require('../webpack/configuration')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const server = new express()
const compiler = webpack(webpackConfiguration)
const staticAbsolutePath = path.resolve(__dirname, '../../demo')

server.set('PORT', process.env.PORT || 8081)
server.use(express.static(staticAbsolutePath))
server.use(webpackDevMiddleware(compiler, {
  publicPath: webpackConfiguration.output.publicPath,
  hot: true
}))
server.use(webpackHotMiddleware(compiler))

server.get('*', function(request, response) {
  response.sendFile(staticAbsolutePath + '/index.html')
})

server.listen(server.get('PORT'), function(error) {
  if (error) {
    return console.log(error)
  }
  console.log('Server executed on %s port.', server.get('PORT'))
})
