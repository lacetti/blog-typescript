const path = require('path')
const plugins = require('./plugins')
const loaders = require('./loaders')
const precss = require('precss')
const autoprefixer = require('autoprefixer')

const nodeEnvironment = process.env.NODE_ENV.replace(/\s+$/, '') || 'development'
const sourceAbsolutePath = path.resolve(__dirname, '../../source')
const environmentDependencies = (function() {
  const environmentDependenciesList = {
    development: [
      'webpack-hot-middleware/client'
    ]
  }

  return (nodeEnvironment in environmentDependenciesList)
    ? environmentDependenciesList[nodeEnvironment]
    : []
}())

module.exports = {
  entry: {
    react: ['react', 'react-dom'],
    application: [
      sourceAbsolutePath + '/bootstrap'
    ].concat(environmentDependencies)
  },
  output: {
    path: path.resolve(__dirname, '../../demo/static/scripts'),
    filename: '[name].js',
    sourceMapFilename: '[name].js.map',
    publicPath: '/static/scripts'
  },
  devtool: nodeEnvironment === 'development' ? 'eval' : 'source-map',
  resolve: {
    root: [sourceAbsolutePath],
    extensions: ['.ts', '.tsx', '.js', ''],
    modulesDirectories: ['node_modules']
  },
  resolveLoader: {
    extensions: ['.js', ''],
    modulesDirectories: ['node_modules'],
    moduleTemplates: ['*-loader']
  },
  plugins: plugins,
  module: {
    preLoaders: loaders.preLoaders,
    loaders: loaders.loaders,
    postLoaders: loaders.postLoaders,
    noParse: loaders.noParse
  },
  postcss: function() {
    return [
      precss(),
      autoprefixer({ browsers: 'last 10 versions' })
    ]
  }
}