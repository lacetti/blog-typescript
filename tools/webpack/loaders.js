module.exports = {
  loaders: [
    {
      test: /\.tsx?$/,
      loaders: ['react-hot', 'babel', 'ts'],
      exclude: /node_modules/
    },
    {
      test: /\.css$/,
      loaders: ['style', 'css', 'postcss']
    },
    {
      test: /\.svg$/,
      loader: 'url'
    }
  ]
}
