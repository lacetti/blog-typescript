const webpack = require('webpack')
const nodeEnvironment = process.env.NODE_ENV.replace(/\s+$/, '') || 'development'
const plugins = {
  general: [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(true),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      filename: 'common.js',
      children: true,
      async: true
    }),
    new webpack.EnvironmentPlugin([
      'NODE_ENV'
    ])
  ],
  development: [
    new webpack.HotModuleReplacementPlugin()
  ],
  production: [
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })
  ]
}

module.exports = (nodeEnvironment in plugins && nodeEnvironment !== 'general')
  ? plugins.general.concat(plugins[nodeEnvironment])
  : plugins.general
