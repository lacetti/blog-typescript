# Global dependencies
Используются следующие глобальные зависимости:

1. typescript 2.1.0-dev.20160718
2. typings 1.3.1
3. webpack 1.13.1

# Installing packages

```bash
# Installing packages
$ npm i -g typescript@next typings webpack

# Link typescript to locale project
$ npm link typescript

# Installing typings modules
$ typings install

# Installing npm modules
$ npm i
```

# NPM tasks documentation
При инициализации npm задачи используются команды следующего вида:
___Группа: Платформа ОС- Команда___: 

```bash
$ npm run development:unix-server
#         ^^^^^^^^^^^ ^^^^ ^^^^^^
#         Группа      ОС   Команда
```

## Development group
Для разработки используется группа npm задач начинающаяся с 
___development___.

## Production group
В production версии используется группа npm задач начинающаяся с 
___production___.

## Unix operation system
При запуске npm задач в операционных системах на платформе unix
необходимо использовать префикс ___unix___.

## Windows operation system
При использовании операционных систем на платформе Microsoft Windows 
необходимо использовать префикс ___windows___.

## Development tasks list

1. server – Инициализация development сервера.

## Production tasks list

1. server - Инициализация production сервера.
2. compile-source - Сборка исходного кода в bundle.
