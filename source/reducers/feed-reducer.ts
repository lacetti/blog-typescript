import {fromJS} from 'immutable'
import * as feedConstants from 'constants/feed-constants'

const initialState = fromJS({
  areUpdatesPending: null,
  updates: {},
  isFeedPending: null,
  feed: []
})

export default function feedReducer(state = initialState, action = {type: '', payload: null}) {
  switch (action.type) {
    case feedConstants.UPDATES_WERE_REQUESTED:
      return state.merge(
        fromJS({
          areUpdatesPending: true
        })
      )

    case feedConstants.UPDATES_WERE_RECEIVED:
      return state.merge(
        fromJS({
          areUpdatesPending: false,
          updates: action.payload
        })
      )

    case feedConstants.UPDATES_WERE_NOT_RECEIVED:
      return state.merge(
        fromJS({
          areUpdatesPending: false
        })
      )

    case feedConstants.FEED_HAS_BEEN_REQUESTED:
      return state.merge(
        fromJS({
          isFeedPending: true
        })
      )

    case feedConstants.FEED_HAS_BEEN_RECEIVED:
      return state.merge(
        fromJS({
          isFeedPending: false,
          feed: action.payload
        })
      )

    case feedConstants.FEED_HAS_NOT_RECEIVED:
      return state.merge(
        fromJS({
          isFeedPending: false
        })
      )
    
    default:
      return state
  }
}
