import * as sessionsConstants from '../constants/sessions-constants'
import {fromJS} from 'immutable'

const initialState = fromJS({
  isAuthorizationPending: false,
  
})

export default function sessionReducer(state = initialState, action = {type: '', payload: null}) {
  switch (action.type) {
    case sessionsConstants.USER_LIST_HAS_BEEN_REQUESTED:
      return state.merge(
        fromJS({
          isAuthorizationPending: true
        })
      )

    case sessionsConstants.USER_LIST_HAS_BEEN_RECEIVED:
      return state.merge(
        fromJS({
          isAuthorizationPending: true
        })
      )
    
    default:
      return state
  }
}
