import {combineReducers} from 'redux'
import sessionsReducer from './sessions-reducer'
import feedReducer from './feed-reducer'

const rootReducer: Redux.Reducer<any> = combineReducers({
  sessions: sessionsReducer,
  feed: feedReducer
})

export default rootReducer
