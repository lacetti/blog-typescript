import * as sessionsConstants from '../constants/sessions-constants'
const ajax = require('ajax-promise')
const apiPath = '/psy-responses/'

export function login(username, password) {
  return function (dispatch) {
    dispatch({
      type: sessionsConstants.USER_LIST_HAS_BEEN_REQUESTED
    })

    ajax.get(apiPath + 'users.json')
      .then(response => {
        response.forEach((
          userPayload: {username: string, password: string},
          iteration: number
        ) => {
          dispatch({
            type: sessionsConstants.USER_LIST_HAS_BEEN_RECEIVED,
            payload: response[iteration]
          })
          
          if (userPayload.username === username && userPayload.password === password) {
            dispatch({
              type: sessionsConstants.USER_HAS_BEEN_LOGGED,
              payload: response[iteration]
            })
          } else {
            dispatch({
              type: sessionsConstants.USER_HAS_NOT_LOGGED
            })
          }  
        })
      })
      .catch(error => {
        dispatch({
          type: sessionsConstants.USER_LIST_HAS_NOT_RECEIVED,
          error: true,
          payload: error || sessionsConstants.USER_LIST_HAS_NOT_RECEIVED
        })
      })
  }
}

export function logout() {
  return function (dispatch) {
    
  }
}
