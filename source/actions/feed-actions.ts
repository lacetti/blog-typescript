import * as feedConstants from '../constants/feed-constants'
const ajax: any = require('ajax-promise')
const apiPath: string = '/psy-responses/'

export function getUpdates() {
  return function(dispatch) {
    dispatch({
      type: feedConstants.UPDATES_WERE_REQUESTED,
      payload: null
    })

    ajax.get(apiPath + 'updates.json')
      .then(response => {
        dispatch({
          type: feedConstants.UPDATES_WERE_RECEIVED,
          payload: response
        })
      })
      .catch(error => {
        dispatch({
          type: feedConstants.UPDATES_WERE_NOT_RECEIVED,
          error: true,
          payload: error || feedConstants.UPDATES_WERE_NOT_RECEIVED
        })
      })
  }
}

export function getFeed() {
  return function(dispatch) {
    dispatch({
      type: feedConstants.FEED_HAS_BEEN_REQUESTED,
      payload: null
    })

    ajax.get(apiPath + 'feed.json')
      .then(response => {
        dispatch({
          type: feedConstants.FEED_HAS_BEEN_RECEIVED,
          payload: response
        })
      })
      .catch(error => {
        dispatch({
          type: feedConstants.FEED_HAS_NOT_RECEIVED,
          error: true,
          payload: error || feedConstants.FEED_HAS_NOT_RECEIVED
        })
      })
  }  
}
