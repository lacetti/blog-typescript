export function uniqueIdentifier(prefix?: string): string {
  return `${prefix && prefix + '#'}${Math.random().toString(36).slice(2)}`
}
