export function stripTags(string: string): string {
  let uriEncodedString: string = encodeURIComponent(string)
  return uriEncodedString.replace(/(%20)+/g, '+').toLowerCase()
}
