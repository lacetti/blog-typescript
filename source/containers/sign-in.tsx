import * as React from 'react'
import SignInComponent from '../components/sign-in'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

class SignIn extends React.Component<any, any> {
  constructor(properties) {
    super(properties)
  }

  render(): JSX.Element {
    return (
      <SignInComponent
        
      />
    )
  }
}

function mapStateToProps(state) {
  return {

  }
}

function mapDispatchToProps(dispatch) {
  return {

  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn)
