import * as React from 'react'
import Template from '../components/template'
import {connect} from 'react-redux'

class Blog extends React.Component<any, any> {
  constructor(properties) {
    super(properties)
  }

  render() {
    return (
      <Template>
        {this.props.children}
      </Template>
    )
  }
}

function mapStateToProps(state) {
  return {

  }
}

function mapDispatchToProps(dispatch) {
  return {

  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Blog)
