import * as React from 'react'
import BlogList from '../components/blog-list'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {getUpdates, getFeed} from 'actions/feed-actions'

interface FeedProperties {
  areUpdatesPending: boolean
  updates: any
  isFeedPending: boolean
  feed: Array<{}>
  getFeed(): any
  getUpdates(): any
}

class Feed extends React.Component<FeedProperties, any> {
  constructor(properties) {
    super(properties)
  }

  componentDidMount() {
    this.props.getFeed()
  }

  render(): JSX.Element {
    return (
      <BlogList
        areUpdatesPending={this.props.areUpdatesPending}
        updates={this.props.updates}
        getUpdates={this.props.getUpdates}
        isFeedPending={this.props.isFeedPending}
        feed={this.props.feed}
      />
    )
  }
}

function mapStateToProps(state) {
  const {feed} = state
  return {
    areUpdatesPending: feed.get('areUpdatesPending'),
    updates: feed.get('updates').toObject(),
    isFeedPending: feed.get('isFeedPending'),
    feed: feed.get('feed').toJS()
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getUpdates: bindActionCreators(getUpdates, dispatch),
    getFeed: bindActionCreators(getFeed, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Feed)
