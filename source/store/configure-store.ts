import {createStore, applyMiddleware} from 'redux'
import rootReducer from 'reducers/root-reducer'
import thunk from 'redux-thunk'
const logger = require('redux-logger')

function configureStoreMiddleware(): Redux.GenericStoreEnhancer {
  let middleware: Redux.Middleware[] = [
    thunk
  ]

  if (typeof process.env.NODE_ENV === 'string' &&
      process.env.NODE_ENV.replace(/\s+$/, '') === 'development') {
    middleware.push(logger())
  }

  return applyMiddleware.apply(null, middleware)
}

export function configureStore(initialState?): Redux.Store<any> {
  const store: Redux.Store<any> = createStore(rootReducer, initialState, configureStoreMiddleware())
  const {hot} = module as any

  if (hot) {
    hot.accept('../reducers/root-reducer', () => {
      store.replaceReducer(require('../reducers/root-reducer'));
    });
  }

  return store
}
