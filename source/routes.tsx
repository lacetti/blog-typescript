import * as React from 'react'
import {Route, IndexRedirect} from 'react-router'
import Blog from 'containers/blog'
import Feed from 'containers/feed'
import SignIn from 'containers/sign-in'

export const routes: JSX.Element = (
  <Route path="/" component={Blog}>
    <IndexRedirect to="feed"/>
    <Route path="feed" component={Feed}/>
    <Route path="sign-in" component={SignIn}/>
  </Route>
)
