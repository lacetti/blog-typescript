import * as React from 'react'
import * as ReactDOM from 'react-dom'
import {configureStore} from 'store/configure-store'
import {Provider} from 'react-redux'
import {Router, browserHistory} from 'react-router'
import {routes} from 'routes'

const store: Redux.Store<any> = configureStore()
const rootElement: HTMLElement = document.getElementById('root')

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes}/>
  </Provider>,
  rootElement
)
