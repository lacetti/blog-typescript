import * as React from 'react'
import './index.css'

export default class SignUpForm extends React.Component<any, void> {
  constructor(properties) {
    super(properties)
  }

  render(): JSX.Element {
    return (
      <div className="sign-up-form">
        <input
          type="text"
          className="sign-up-form__field"
          placeholder="Your E-Mail address"
        />
        <button type="button" className="sign-up-form__button">
          Create Account
        </button>
      </div>
    )
  }
}
