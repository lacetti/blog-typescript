import * as React from 'react'
import './index.css'

export default class SignIn extends React.Component<any, any> {
  constructor(properties) {
    super(properties)
  }
  
  render(): JSX.Element {
    return (
      <div className="sign-in">
        <div className="sign-in__form">
          <input
            type="text"
            className="sign-in__field"
            placeholder="Your E-Mail address"
          />
          <input
            type="text"
            className="sign-in__field"
            placeholder="Your password"
          />
          <button type="button" className="sign-in__button">
            Sign In
          </button>
        </div>
      </div>
    )
  }
}
