import * as React from 'react'
import {Link} from 'react-router'
import {stripTags} from 'helpers/strip-tags'
import './index.css'

interface BlogProperties {
  identifier: number
  avatar?: string
  author: string
  theme?: string
  message: string
  date: string
  likeScore: number
}

export default class Blog extends React.Component<BlogProperties, any> {
  constructor(properties) {
    super(properties)
  }

  render(): JSX.Element {
    const {avatar, author} = this.props
    return (
      <div className="blog">
        <div className="blog__header">
          <div className="blog__avatar">
            {typeof avatar === 'string'
              ? <img src={avatar}/>
              : (Array.prototype.slice.call(author))[0]
            }
          </div>
          <div className="blog__author">
            {author}
          </div>
          <div className="blog__date">
            {this.props.date}
          </div>
        </div>
        <div className="blog__message">
          <div className="blog__theme">
            {this.props.theme}
          </div>
          {this.props.message}
        </div>
        <div className="blog__referrers">
          <button className="blog__like-link">
            <span className="blog__like">
            </span>
            <span className="blog__like-score">
              {this.props.likeScore}
            </span>
          </button>
          <Link to={`/${stripTags(author)}/${stripTags(this.props.theme)}`} className="blog__link">
            Read more
          </Link>
        </div>
      </div>
    )
  }
}
