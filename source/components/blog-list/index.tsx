import * as React from 'react'
import Caption from '../caption'
import {uniqueIdentifier} from 'helpers/unique-identifier'
import Blog from '../blog'
import SignUpForm from '../sign-up-form'
import './index.css'

interface BlogListProperties {
  areUpdatesPending: boolean
  updates: any
  isFeedPending: boolean
  feed: any[]
  getUpdates(): any
}

export default class BlogList extends React.Component<BlogListProperties, void> {
  constructor(properties) {
    super(properties)
  }

  private renderUpdates(): JSX.Element {
    if (typeof this.props.areUpdatesPending === 'boolean' && !this.props.areUpdatesPending) {
      const {updates} = this.props
      return (
        <Blog
          identifier={updates.identifier}
          author={updates.author}
          date={updates.date}
          theme={updates.theme}
          message={updates.message}
          likeScore={updates.like_score}
        />
      )
    }
  }

  private renderFeed(): JSX.Element[] {
    if (typeof this.props.isFeedPending === 'boolean' && !this.props.isFeedPending) {
      return this.props.feed.map(blog =>
        <Blog
          key={uniqueIdentifier('blog')}
          identifier={blog.identifier}
          author={blog.author}
          avatar={blog.avatar}
          date={blog.date}
          theme={blog.theme}
          message={blog.message}
          likeScore={blog.like_score}
        />
      )
    }
  }

  render(): JSX.Element {
    const updatesComponent = this.renderUpdates() || null
    const feedComponents = this.renderFeed() || null
    return (
      <div className="blog-list">
        <Caption className="blog-list__caption" action={this.props.getUpdates}>
          <div className="caption__content">
            <div className="caption__title">
              Write your own blog with Bloggify blogger place.
            </div>
            <div className="caption__description">
              Create your account in 55 seconds.
            </div>
            <SignUpForm/>
          </div>
          <div className="caption__last-blog">
            {updatesComponent}
          </div>
        </Caption>
        <div className="blog-list-filter">
        </div>
        <div className="blog-list__feed">
          {feedComponents}
        </div>
      </div>
    )
  }
}
