import * as React from 'react'
import NavigationTab from './tab'
import {uniqueIdentifier} from 'helpers/unique-identifier'
import './index.css'

interface NavigationTabProperties {
  identifier: string,
  label: string,
  path: string
}

interface NavigationProperties {
  tabs: NavigationTabProperties[]
}

class Navigation extends React.Component<NavigationProperties, void> {
  constructor(properties) {
    super(properties)
  }

  private renderTabs(): JSX.Element[] {
    const generalThemeList = {
      "sign-in": true,
      "sign-up": true
    }
    return this.props.tabs.map(tab =>
      <NavigationTab
        key={uniqueIdentifier('navigation')}
        path={tab.path}
        label={tab.label}
        themeName={tab.identifier in generalThemeList && tab.identifier}
        onlyActiveOnIndex={tab.identifier === 'feed'}
      />
    )
  }
  
  render(): JSX.Element {
    const tabs = this.renderTabs()
    return (
      <div className="navigation">
        {tabs}
      </div>
    )
  }
}

export default Navigation
