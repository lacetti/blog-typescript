import * as React from 'react'
import {Link} from 'react-router'
import classNames from 'classnames'
import './index.css'

interface NavigationTabProperties {
  path: string,
  label: string,
  themeName: string,
  onlyActiveOnIndex: boolean
}

class NavigationTab extends React.Component<NavigationTabProperties, void> {
  constructor(properties) {
    super(properties)
  }

  render(): JSX.Element {
    const navigationTabClassNames = classNames({
      'navigation__tab': true,
      [`navigation__tab_theme_${this.props.themeName}`]: !!this.props.themeName
    })
    return (
      <Link
        to={this.props.path}
        className={navigationTabClassNames}
        activeClassName="navigation__tab_current"
      >
        {this.props.label}
      </Link>
    )
  }
}

export default NavigationTab
