import * as React from 'react'
import classNames from 'classnames'
import './index.css'

interface CaptionProperties {
  captionClosed?: boolean
  className?: string
  action?: any
}

export default class Caption extends React.Component<CaptionProperties, any> {
  constructor(properties) {
    super(properties)
  }

  componentDidMount() {
    const {action} = this.props
    if (typeof action === 'function') {
      action()
    }
  }

  render(): JSX.Element {
    const {captionClosed} = this.props
    const captionClassNames = classNames({
      'caption': true,
      [this.props.className]: !!this.props.className
    })

    if ((typeof captionClosed === 'boolean' && captionClosed) || typeof captionClosed === 'undefined') {
      return (
        <div className={captionClassNames}>
          <button className="caption__close">
          </button>
          {this.props.children}
        </div>
      )
    } else {
      return null
    }
  }
}
