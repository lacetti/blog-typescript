import * as React from 'react'
import Header from '../header'
import './index.css'

export default class Template extends React.Component<any, any> {
  constructor(properties) {
    super(properties)
  }

  render(): JSX.Element {
    return (
      <div className="page">
        <Header/>
        {this.props.children}
      </div>
    )
  }
}
