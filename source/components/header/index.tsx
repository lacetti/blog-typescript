import * as React from 'react'
import {IndexLink} from 'react-router'
import Navigation from '../navigation'
import './index.css'

export default class Header extends React.Component<any, any> {
  constructor(properties) {
    super(properties)
  }

  render(): JSX.Element {
    return (
      <div className="header">
        <IndexLink to="/" className="logotype">
          Bloggify
        </IndexLink>
        <Navigation
          tabs={[
            {
              identifier: 'feed',
              label: 'Feed',
              path: '/feed'
            },
            {
              identifier: 'sign-in',
              label: 'Sign In',
              path: '/sign-in'
            },
            {
              identifier: 'sign-up',
              label: 'Sign Up',
              path: '/sign-up'
            }
          ]}
        />
      </div>
    )
  }
}
